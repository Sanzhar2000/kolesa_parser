from typing import List

from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session
import requests
from . import crud, models, schemas
from .database import SessionLocal, engine

from parser.parser import parse
from .models import Item

import telebot, time

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
bot = telebot.TeleBot('1970522200:AAHf--N8XObFGUep8Jovr9Dqp9GnYMVSiug')
chat_id = 385529421  # chat id telegram


# bot.polling(none_stop=True, interval=1)

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def get_list_diff_photos(arr1, arr2):  # arr1 - initial list, arr2 - final list
    arr1_photos = []
    for i in arr1:
        arr1_photos.append(i.link)

    added = list(set(arr2) - set(arr1_photos))
    removed = list(set(arr1_photos) - set(arr2))
    if not added and not removed:
        return None

    return {
        'added': added,
        'removed': removed,
    }


def get_list_diff_equipments(arr1, arr2):  # arr1 - initial list, arr2 - final list
    arr1_equipments = []
    for i in arr1:
        arr1_equipments.append(i.name)

    added = list(set(arr2) - set(arr1_equipments))
    removed = list(set(arr1_equipments) - set(arr2))
    if not added and not removed:
        return None

    return {
        'added': added,
        'removed': removed,
    }


@app.get("/")
# @bot.message_handler(content_types=['text'])
def root(db: Session = Depends(get_db)):
    x, photos, equipments = parse()  # x = main info of item

    diff = []

    if crud.get_item(db, x["kolesa_id"]):
        old_version = crud.get_item(db, x["kolesa_id"])
        # att = "brand"
        # print(getattr(old_version, att))

        attributes = [
            "brand", "name", "year", "price",
            "city", "car_body", "volume", "mileage",
            "transmission", "wheel", "colour", "drive",
            "is_customs_cleared", "vin", "description",
        ]

        for attr in attributes:
            if getattr(old_version, attr) != x[attr]:  # BRAND
                old_value = getattr(old_version, attr)
                setattr(old_version, attr, x[attr])
                sample_diff = attr + " old value: " + old_value + ", new value: " + x[attr]
                diff.append(sample_diff)

        if get_list_diff_photos(old_version.photos, photos) is not None:
            # delete old version
            # crete new photos in db
            res = get_list_diff_photos(old_version.photos, photos)
            if res["removed"]:
                arr = ["Removed photos: "]
                for removed in res["removed"]:
                    string = "Removed: " + removed
                    arr.append(string)
                diff = diff + arr
                crud.delete_all_photos(db, old_version.id)
            if res["added"]:
                arr = ["Added photos: "]
                for added in res["added"]:
                    string = "Added: " + added
                    arr.append(string)
                    crud.create_photo(db, old_version.id, added)
                diff = diff + arr
        if get_list_diff_equipments(old_version.equipments, equipments) is not None:
            # delete old version
            # crete new equipments in db
            res = get_list_diff_equipments(old_version.equipments, equipments)
            if res["removed"]:
                arr = ["Removed equipments: "]
                for removed in res["removed"]:
                    string = "Removed: " + removed
                    arr.append(string)
                diff = diff + arr
                crud.delete_all_equipments(db, old_version.id)
            if res["added"]:
                arr = ["Added equipments: "]
                for added in res["added"]:
                    string = "Added: " + added
                    arr.append(string)
                    crud.create_equipment(db, old_version.id, added)
                diff = diff + arr

        db.add(old_version)
        db.commit()

        if diff:
            for i in diff:
                bot.send_message(chat_id, i)

        return diff
    else:
        new_item = Item(
            kolesa_id=x["kolesa_id"],
            brand=x["brand"],
            name=x["name"],
            year=x["year"],
            price=x["price"],
            city=x["city"],
            car_body=x["car_body"],
            volume=x["volume"],
            mileage=x["mileage"],
            transmission=x["transmission"],
            wheel=x["wheel"],
            colour=x["colour"],
            drive=x["drive"],
            is_customs_cleared=x["is_customs_cleared"],
            vin=x["vin"],
            description=x["description"],
        )

        # crud.create_item(db, new_item)
        db.add(new_item)
        db.commit()

        for photo in photos:
            crud.create_photo(db, new_item.id, photo)

        for equipment in equipments:
            crud.create_equipment(db, new_item.id, equipment)

        return new_item
