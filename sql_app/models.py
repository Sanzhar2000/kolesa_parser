from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from .database import Base


class Item(Base):
    __tablename__ = "items"

    id = Column(Integer, primary_key=True, index=True)

    kolesa_id = Column(String(50))

    brand = Column(String(50))
    name = Column(String(50))
    year = Column(String(50))
    price = Column(String(50))

    city = Column(String(50))
    car_body = Column(String(50))
    volume = Column(String(50))
    mileage = Column(String(50))
    transmission = Column(String(50))
    wheel = Column(String(50))
    colour = Column(String(50))
    drive = Column(String(50))
    is_customs_cleared = Column(String(50))
    vin = Column(String(50))

    # title = Column(String, index=True)
    description = Column(String)

    photos = relationship("Photo", back_populates="item")
    equipments = relationship("Equipment", back_populates="item")


class Photo(Base):
    __tablename__ = "photos"

    item_id = Column(Integer, ForeignKey("items.id"))
    id = Column(Integer, primary_key=True)
    link = Column(String(200))

    item = relationship("Item", back_populates="photos")


class Equipment(Base):
    __tablename__ = "equipments"

    item_id = Column(Integer, ForeignKey("items.id"))
    id = Column(Integer, primary_key=True)
    name = Column(String(200))

    item = relationship("Item", back_populates="equipments")
