from sqlalchemy.orm import Session

from . import models, schemas


def get_items(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Item).offset(skip).limit(limit).all()


def create_item(db: Session, item: schemas.ItemCreate):
    db_item = models.Item(**item.dict())
    db.add(db_item)
    db.commit()
    db.refresh(db_item)
    return db_item


def get_item(db: Session, kolesa_id: str):
    try:
        item = db.query(models.Item).filter(models.Item.kolesa_id == kolesa_id).first()
    except:
        return None

    return item


def create_photo(db: Session, item_id: int, link: str):
    db_photo = models.Photo(link=link, item_id=item_id)
    db.add(db_photo)
    db.commit()
    db.refresh(db_photo)
    return db_photo


def get_photo(db: Session, id: int):
    return db.query(models.Photo).filter(models.Photo.id == id).first()


def photo_list(db: Session):
    return db.query(models.Photo).all()


def delete_all_photos(db: Session, item_id: int):
    return db.query(models.Photo).filter(models.Photo.item_id == item_id).delete()


def create_equipment(db: Session, item_id: int, name: str):
    db_equipment = models.Equipment(name=name, item_id=item_id)
    db.add(db_equipment)
    db.commit()
    db.refresh(db_equipment)
    return db_equipment


def delete_all_equipments(db: Session, item_id: int):
    return db.query(models.Equipment).filter(models.Equipment.item_id == item_id).delete()
