from typing import Optional

from pydantic import BaseModel


# Item
class ItemBase(BaseModel):
    kolesa_id: str

    brand: str
    name: str
    year: str
    price: str

    city: str
    car_body: str
    volume: str
    mileage: str
    transmission: str
    wheel: str
    colour: str
    drive: str
    is_customs_cleared: str
    vin: str

    # photos: Optional[str] = None
    # equipments: Optional[str] = None

    description: Optional[str] = None


class ItemCreate(ItemBase):
    pass


class Item(ItemBase):
    id: int

    class Config:
        orm_mode = True


# Photo
class PhotoBase(BaseModel):
    link: str


class PhotoCreate(PhotoBase):
    pass


class Photo(PhotoBase):
    id: int

    class Config:
        orm_mode = True


class PhotoList(PhotoBase):
    item_id: int
    item: Item

    class Config:
        orm_mode = True


# Equipment
class EquipmentBase(BaseModel):
    name: str


class EquipmentCreate(EquipmentBase):
    pass


class Equipment(EquipmentBase):
    id: int

    class Config:
        orm_mode = True
