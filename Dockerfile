FROM python:3.9.4-slim-buster

COPY ./sql_app /tmp/sql_app
COPY ./alembic.ini /tmp
COPY ./alembic /tmp/alembic
COPY ./parser /tmp/parser
COPY ./requirements.txt /tmp

WORKDIR /tmp

RUN pip install -r requirements.txt
#RUN alembic init alembic
RUN alembic upgrade head

EXPOSE 8000

CMD ["uvicorn", "sql_app.main:app", "--host=0.0.0.0", "--reload"]