from bs4 import BeautifulSoup
import requests
from unicodedata import normalize

from urllib.parse import urlparse, unquote
from pathlib import PurePosixPath


def link_header(item: BeautifulSoup):
    return item.find('header', class_='offer__header').find('h1', class_='offer__title')


def get_sidebar_dls(item: BeautifulSoup):
    return item.find('section', 'offer__container') \
        .find('div', class_='offer__sidebar') \
        .find('div', class_='offer__sidebar-info') \
        .find('div', class_='offer__parameters') \
        .findAll('dl')


def get_offer_content(item: BeautifulSoup):
    return item.find('section', 'offer__container').find('div', class_='offer__content')


def get_brand(item: BeautifulSoup) -> str:
    initial = link_header(item)
    brand = initial.find('span', itemprop='brand').getText(strip=True)

    return brand


def get_name(item: BeautifulSoup) -> str:
    initial = link_header(item)
    name = initial.find('span', itemprop='name').getText(strip=True)

    return name


def get_year(item: BeautifulSoup) -> str:
    initial = link_header(item)
    year = initial.find('span', class_='year').getText(strip=True)

    return year


def get_price(item: BeautifulSoup) -> str:
    price = item.find('section', 'offer__container') \
        .find('div', class_='offer__sidebar') \
        .find('div', class_='offer__sidebar-info') \
        .find('div', class_='offer__sidebar-header') \
        .find('div', class_='offer__price') \
        .getText(strip=True).lstrip("₸")

    return price


def get_city(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_city = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', {"class": "value-title", "title": "Город"}):
            if dl.find('dt', {"class": "value-title", "title": "Город"}).getText(strip=True) == 'Город':
                index_of_city = i
                break

    if index_of_city == -1:
        return 'null'

    city = dls[index_of_city].find('dd', class_='value').getText(strip=True)

    return city


def get_car_body(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_car_body = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', {"class": "value-title", "title": "Кузов"}):
            if dl.find('dt', {"class": "value-title", "title": "Кузов"}).getText(strip=True) == 'Кузов':
                index_of_car_body = i
                break

    if index_of_car_body == -1:
        return 'null'

    body = dls[index_of_car_body].find('dd', class_='value').getText(strip=True)

    return body


def get_volume(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_volume = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', class_='value-title', title='Объем двигателя, л'):
            if dl.find('dt', class_='value-title', title='Объем двигателя, л').getText(
                    strip=True) == 'Объем двигателя, л':
                index_of_volume = i
                break

    if index_of_volume == -1:
        return 'null'

    volume = dls[index_of_volume].find('dd', class_='value').getText(strip=True)

    return volume


def get_mileage(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_mileage = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', class_='value-title', title='Пробег'):
            if dl.find('dt', class_='value-title', title='Пробег').getText(strip=True) == 'Пробег':
                index_of_mileage = i
                break

    if index_of_mileage == -1:
        return 'null'

    mileage = dls[index_of_mileage].find('dd', class_='value').getText(strip=True)

    return mileage


def get_transmission(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_transmission = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', class_='value-title', title='Коробка передач'):
            if dl.find('dt', class_='value-title', title='Коробка передач').getText(strip=True) == 'Коробка передач':
                index_of_transmission = i
                break

    if index_of_transmission == -1:
        return 'null'

    transmission = dls[index_of_transmission].find('dd', class_='value').getText(strip=True)

    return transmission


def get_wheel(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_wheel = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', class_='value-title', title='Руль'):
            if dl.find('dt', class_='value-title', title='Руль').getText(strip=True) == 'Руль':
                index_of_wheel = i
                break

    if index_of_wheel == -1:
        return 'null'

    wheel = dls[index_of_wheel].find('dd', class_='value').getText(strip=True)

    return wheel


def get_colour(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_colour = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', class_='value-title', title='Цвет'):
            if dl.find('dt', class_='value-title', title='Цвет').getText(strip=True) == 'Цвет':
                index_of_colour = i
                break

    if index_of_colour == -1:
        return 'null'

    colour = dls[index_of_colour].find('dd', class_='value').getText(strip=True)

    return colour


def get_drive(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_drive = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', class_='value-title', title='Привод'):
            if dl.find('dt', class_='value-title', title='Привод').getText(strip=True) == 'Привод':
                index_of_drive = i
                break

    if index_of_drive == -1:
        return 'null'

    drive = dls[index_of_drive].find('dd', class_='value').getText(strip=True)

    return drive


def get_is_customs_cleared(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_is_customs_cleared = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', class_='value-title', title='Растаможен в Казахстане'):
            if dl.find('dt', class_='value-title', title='Растаможен в Казахстане').getText(
                    strip=True) == 'Растаможен в Казахстане':
                index_of_is_customs_cleared = i
                break

    if index_of_is_customs_cleared == -1:
        return 'null'

    is_customs_cleared = dls[index_of_is_customs_cleared].find('dd', class_='value').getText(strip=True)

    return is_customs_cleared


def get_vin(item: BeautifulSoup) -> str:
    dls = get_sidebar_dls(item)
    index_of_vin = -1

    for i, dl in enumerate(dls, start=0):
        if dl.find('dt', class_='value-title', title='VIN'):
            if dl.find('dt', class_='value-title', title='VIN').getText(strip=True) == 'VIN':
                index_of_vin = i
                break

    if index_of_vin == -1:
        return 'null'

    vin = dls[index_of_vin].find('dd', class_='value').getText(strip=True)

    return vin


# def get_phone(item: BeautifulSoup) -> str:
#     phone = item.find('section', 'offer__container').find('div', class_='offer__sidebar hasNoCredit').find('div', class_='offer__sidebar-sticky-wrap').find('div', class_='offer__sidebar-contacts-wrap').find('div', class_='offer__sidebar-contacts').find('div', class_='offer__contacts').find('div', 'offer__phones').find('div', class_='seller-phones').find('div', 'seller-phones__prefix isBlurred').getText(strip=True)
#     return phone

def get_photos(item: BeautifulSoup) -> list:
    initial = get_offer_content(item)
    results = initial.find('div', class_='offer__gallery gallery') \
        .find('ul', class_='gallery__thumbs-list js__gallery-thumbs') \
        .findAll('img')

    photos = []
    for result in results:
        photos.append(result['src'])

    return photos


def get_equipments(item: BeautifulSoup) -> list:
    initial = get_offer_content(item)
    results = initial.find('div', class_='offer__description').find_all('div', class_='text')
    if len(results) != 2 and initial.find('div', class_='offer__description').find('h3', class_='offer__sub'):
        return []

    result = results[0].find('p').getText(strip=True)
    equipments = result.split(", ")

    return equipments


def get_description(item: BeautifulSoup) -> str:
    initial = get_offer_content(item)
    results = initial.find('div', 'offer__description').find_all('div', class_='text')

    if len(results) != 2 and initial.find('div', class_='offer__description').find('h3', class_='offer__sub'):
        description = results[0].find('p').getText(strip=True)
    else:
        description = results[1].find('p').getText(strip=True)

    return description


def parse():
    url = 'https://kolesa.kz/a/show/125294232'
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) '
                      'Version/14.1.2 Safari/605.1.15 '
    }

    response = requests.get(url, headers=headers)
    soup = BeautifulSoup(response.content, 'html.parser')
    item = soup.find('div', class_='offer')

    kolesa_id = PurePosixPath(
                    unquote(
                        urlparse(
                            url
                        ).path
                    )
                ).parts[3]

    item_parsed = {
        'kolesa_id': kolesa_id,
        'brand': get_brand(item),
        'name': get_name(item),
        'year': get_year(item),
        'price': normalize('NFKD', get_price(item)),

        'city': get_city(item),
        'car_body': get_car_body(item),
        'volume': get_volume(item),
        'mileage': get_mileage(item),
        'transmission': get_transmission(item),
        'wheel': get_wheel(item),
        'colour': get_colour(item),
        'drive': get_drive(item),
        'is_customs_cleared': get_is_customs_cleared(item),
        'vin': get_vin(item),

        # 'phone': get_phone(item),

        # 'photos': get_photos(item),
        # 'equipments': get_equipments(item),
        'description': get_description(item),
    }

    photos_parsed = get_photos(item)

    equipments_parsed = get_equipments(item)

    return item_parsed, photos_parsed, equipments_parsed
