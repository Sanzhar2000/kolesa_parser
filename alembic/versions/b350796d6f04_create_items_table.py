"""create items table

Revision ID: b350796d6f04
Revises: 
Create Date: 2021-09-08 12:54:40.149795

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'b350796d6f04'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'items',
        sa.Column('id', sa.Integer, primary_key=True),

        sa.Column('kolesa_id', sa.String(50), nullable=False),

        sa.Column('brand', sa.String(50), nullable=False),
        sa.Column('name', sa.String(50), nullable=False),
        sa.Column('year', sa.String(50)),
        sa.Column('price', sa.String(50)),

        sa.Column('city', sa.String(50)),
        sa.Column('car_body', sa.String(50)),
        sa.Column('volume', sa.String(50)),
        sa.Column('mileage', sa.String(50)),
        sa.Column('transmission', sa.String(50)),
        sa.Column('wheel', sa.String(50)),
        sa.Column('colour', sa.String(50)),
        sa.Column('drive', sa.String(50)),
        sa.Column('is_customs_cleared', sa.String(50)),
        sa.Column('vin', sa.String(50)),

        sa.Column('description', sa.Unicode(200))
    )


def downgrade():
    pass
