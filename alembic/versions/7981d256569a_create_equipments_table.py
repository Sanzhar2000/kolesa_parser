"""create equipments table

Revision ID: 7981d256569a
Revises: ed00d3df55fb
Create Date: 2021-09-08 19:09:40.185848

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7981d256569a'
down_revision = 'ed00d3df55fb'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'equipments',
        sa.Column('item_id', sa.Integer),

        sa.Column('id', sa.Integer, primary_key=True),

        sa.Column('name', sa.String(50), nullable=True)
    )


def downgrade():
    pass
