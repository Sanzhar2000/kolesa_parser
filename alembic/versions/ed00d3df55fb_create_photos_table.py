"""create photos table

Revision ID: ed00d3df55fb
Revises: b350796d6f04
Create Date: 2021-09-08 18:51:17.336355

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ed00d3df55fb'
down_revision = 'b350796d6f04'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'photos',
        sa.Column('item_id', sa.Integer),

        sa.Column('id', sa.Integer, primary_key=True),

        sa.Column('link', sa.String(50), nullable=True)
    )


def downgrade():
    pass
