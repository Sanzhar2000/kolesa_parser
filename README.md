# Kolesa Parser
## _Test Task_


It is a web scrapper from kolesa.kz web-site that will send to your telegram the changes made in selected car sale announcement.

This app was built using FastAPI.

- Python 3.9.6
- Fast API
- SQLite (easily may upgraded to any relational databasee)
- SQLAlchemy ORM, alembic migrations
- bs4 library for scrapper

## Installation

This app requires [Python 3 .9] to run.

Install the dependencies and devDependencies and start the server.

```sh
cd "this directory"
python3 -m venv venv # create virtual environment 
source venv/bin/activate #  (for Linux and Mac)
pip install -r requirements.txt
# configute your db credentials if necessary
alembic upgrade head # (migrate tables)
uvicorn sql_app.main:app --reload # run server
```

# Enjoy!!!

## License

MIT

**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>